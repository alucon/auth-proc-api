##############################

##########auth-proc-api#########

##############################

###########version#1.0.0#########

##############################

######author#Lucian Constantin#####

##############################


About
This is a sample Mulesoft process API. It connects to a MySQL server database that contains registered users.
It listens on the /login endpoint for an HTTP POST method that must contain a Authorization header with Basic Authentication credentials.
After the header creation, it caches is it for 30 mins, during which the API consumer can validate it using the HTTP HEAD method.

Run instructions:
To run the project configure an environment variable named "mule.key" with the value "DataMorphosis01!".
Also, you need to provide your database credentials in the dm-auth-proc-api-{{env}}.properties file.

This project contains:
- Mulesoft XML files that describe the data flow
- API RAML specification
- basic MUnit testing
- Java code samples for password encryption and JWT creation
- Properties and secure properties 
- Postmaan collection for calling the API
- SQL script used to create the database and insert a user