package com.data.morphosis.authentication;
import java.util.HashMap;

import javax.xml.bind.DatatypeConverter;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;


public class DecryptJWT {

	//Sample method to validate and read the JWT
	public static HashMap parseJWT(String jwt, String secret) {
	 
	    //This line will throw an exception if it is not a signed JWS (as expected)
	    Claims claims = Jwts.parser()         
	       .setSigningKey(DatatypeConverter.parseBase64Binary(secret))
	       .parseClaimsJws(jwt).getBody();
	    HashMap<String, Comparable> map = new HashMap();
	    map.put("id", claims.getId());
	    map.put("subject", claims.getSubject());
	    map.put("expiration", claims.getExpiration());
	    map.put("issuer", claims.getIssuer());
	    return map;
	}
}
