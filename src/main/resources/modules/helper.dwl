%dw 2.0
import * from dw::core::Binaries 
fun extractUsername(str)=(fromBase64(str replace "Basic " with "") splitBy ":" )[0]
fun extractPassword(str)=(fromBase64(str replace "Basic " with "") splitBy ":" )[1]