CREATE DATABASE userdirectory;
USE userdirectory;

CREATE TABLE `users` (
	`ID` bigint NOT NULL AUTO_INCREMENT,
	`FIRSTNAME` varchar(255) NOT NULL,
	`LASTNAME` varchar(255) NOT NULL,
	`FULLTNAME` varchar(255) NOT NULL,
    `USERNAME` varchar(255) NOT NULL,
	`PASSWORD` varchar(255) NOT NULL,
	`CREATED` DATETIME NOT NULL,
	`MODIFIED` DATETIME NOT NULL,
	PRIMARY KEY (`ID`)
);

INSERT INTO users (FIRSTNAME, LASTNAME, FULLTNAME, USERNAME, PASSWORD, CREATED, MODIFIED)
VALUES ('Lucian', 'Constantin', 'Lucian Andrei Constantin', 'lucian.constantin.work@gmail.com','3babddfb33fefdec545fff5b4adaba88dcc5d3df6ca2e922793a0ac3ed7dbfecc9c2ea43ed784a7982041fb68a8b7032f6f576ee98f05e0cef9bf49ca233e8a1', '2020-03-01 18:00:00', '2020-03-01 18:00:00')
